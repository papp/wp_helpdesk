<?
class wp_helpdesk__admin__helpdesksetting extends wp_helpdesk__admin__helpdesksetting__parent
{
	function load($d = null)
	{
		parent::{__function__}();
		$this->C->user()->check_right(['RIGHT'=>'ADMIN']);
		switch($this->D['ACTION'])
		{
			case 'add_status':
				$this->D['SETTING']['HELPDESK']['D']['STATUS'] = null;
				$this->D['SETTING']['HELPDESK']['D']['STATUS']['D'][time()]['ACTIVE'] = 1;
				break;
			case 'add_priority':
				$this->D['SETTING']['HELPDESK']['D']['PRIORITY'] = null;
				$this->D['SETTING']['HELPDESK']['D']['PRIORITY']['D'][time()]['ACTIVE'] = 1;
				break;
			case 'add_prior':
				$this->D['SETTING']['HELPDESK']['D']['STATUS'] = null;
				$this->D['SETTING']['HELPDESK']['D']['STATUS']['D'][time()]['ACTIVE'] = 1;
				break;
			case 'add_emailbox':
				$this->D['SETTING']['HELPDESK']['ACCOUNT']['D'] = null;
				$this->D['SETTING']['HELPDESK']['ACCOUNT']['D'][time()]['ACTIVE'] = 1;
				$this->C->helpdesk()->get_group();
				break;
			case 'add_group':
				#$this->C->helpdesk()->get_group();
				break;
			case 'add_textblock':
				$this->C->helpdesk()->get_group();
				break;
			case 'set_setting':
				foreach((array)$this->D['SETTING']['HELPDESK']['D']['PRIORITY']['D'] AS $k => $v)
					if($v['ACTIVE'] == '-2')
						unset($this->D['SETTING']['HELPDESK']['D']['PRIORITY']['D'][$k]);
					
				foreach((array)$this->D['SETTING']['HELPDESK']['D']['STATUS']['D'] AS $k => $v)
					if($v['ACTIVE'] == '-2')
						unset($this->D['SETTING']['HELPDESK']['D']['STATUS']['D'][$k]);
		
				foreach((array)$this->D['SETTING']['HELPDESK']['ACCOUNT']['D'] AS $k => $v)
					if($v['ACTIVE'] == '-2')
						unset($this->D['SETTING']['HELPDESK']['ACCOUNT']['D'][$k]);
				
				#$d['MODUL']['D']['wp_setting']['SETTING'] = $this->D['SETTING']['HELPDESK'];
				$this->C->setting()->set_setting();
				$this->C->helpdesk()->set_group();
				$this->C->helpdesk()->set_textblock();
				exit;
				break;
			default:
				$this->C->setting()->get_setting();
				$this->C->helpdesk()->get_group();
				$this->C->helpdesk()->get_textblock();
				#$this->D['USER']['W'] = ['RIGHT_ID' => 'TICKET', 'RIGHT' => '7770'];
				$this->C->user()->get_user();
				break;
		}
	}
	
	function show($d=null)
	{
		$this->C->library()->smarty()->assign('D', $this->D);
		$this->C->library()->smarty()->display('admin__helpdesksetting.tpl');
	}
}