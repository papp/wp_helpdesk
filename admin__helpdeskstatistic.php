<?
class wp_helpdesk__admin__helpdeskstatistic extends wp_helpdesk__admin__helpdeskstatistic__parent
{
	function load($d = null)
	{
		parent::{__function__}();
		
		$this->C->user()->check_right(['RIGHT'=>'ADMIN']);
		switch($this->D['ACTION'])
		{
			default:
				$this->C->helpdesk()->get_ticket();
				break;
		}
	}
	
	function show($d=null)
	{
		$this->C->library()->smarty()->assign('D', $this->D);
		$this->C->library()->smarty()->display(__dir__.'/tpl/admin__helpdeskstatistic.tpl');
	}
}