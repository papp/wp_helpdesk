<?
class wp_helpdesk__class__helpdesk extends wp_helpdesk__class__helpdesk__parent
{
	function __construct(&$D = null)
	{
		parent::{__function__}($D);
    }
	
	function send_mail()
	{
		if($this->D['HELPDESK']['D']['TICKET']['D'])
		{
			$this->C->setting()->get_setting();
			
			foreach($this->D['HELPDESK']['D']['TICKET']['D'] as $k => $v )
			{
				foreach($v['MESSAGE']['D'] as $k1 => $v1 )
				{
					if($v1['TO'])
					{
						$ACCOUNT = $this->D['SETTING']['HELPDESK']['ACCOUNT']['D'][ $v1['FROM_ID'] ];
						#sendmail
						$mail = [
							'CONNECT'		=> [ 'METHOD' => 'SMTP', 'AUTH'=>1, 'HOST' => $ACCOUNT['OUT']['SERVER'],'USERNAME' => $ACCOUNT['OUT']['USER'], 'PASSWORD' => $ACCOUNT['OUT']['PASSWORD'] ],
							'FROM'			=> ['EMAIL' => $ACCOUNT['OUT']['EMAIL'] ],
							'TO'			=> [ ['EMAIL' => $v1['TO'] ] ],
							'MAIL'			=> ['SUBJECT' => $v1['TITLE'], 'BODY'  => $v1['TEXT'] ],
						];
					
					foreach((array)$v1['FILE_SEONAME']['D'] as $k2 => $v2 )
					{
						$mail['ATTACHMENT'][] = $v2;
					}
					
					$isSend = $this->C->mail()->send($mail);
					
						$this->D['HELPDESK']['D']['TICKET']['D'][$k]['MESSAGE']['D'][$k1]['ACTIVE'] = ($isSend)?1:-1;
						
						if($isSend)
						{
							$this->D['HELPDESK']['D']['TICKET']['D'][$k]['MESSAGE']['D'][$k1]['USER_ID'] = $this->D['SESSION']['USER']['ID'];
							$this->D['HELPDESK']['D']['TICKET']['D'][$k]['MESSAGE']['D'][$k1]['FROM'] = $ACCOUNT['OUT']['EMAIL'];
							$this->D['HELPDESK']['D']['TICKET']['D'][$k]['MESSAGE']['D'][$k1]['DATETIME'] = date('YmdHis');
							$this->D['HELPDESK']['D']['TICKET']['D'][$k]['MESSAGE']['D'][$k1]['WAY'] = 'out';
							$this->D['HELPDESK']['D']['TICKET']['D'][$k]['DATETIME'] = date('YmdHis');
							$this->D['HELPDESK']['D']['TICKET']['D'][$k]['ACTIVE'] = 0;
						}
					}
					else #Notice
					{
						$this->D['HELPDESK']['D']['TICKET']['D'][$k]['MESSAGE']['D'][$k1]['USER_ID'] = $this->D['SESSION']['USER']['ID'];
						#$this->D['HELPDESK']['D']['TICKET']['D'][$k]['MESSAGE']['D'][$k1]['TITLE'] = 'NOTICE';
						#$this->D['HELPDESK']['D']['TICKET']['D'][$k]['DATETIME'] = date('YmdHis');
						$this->D['HELPDESK']['D']['TICKET']['D'][$k]['MESSAGE']['D'][$k1]['DATETIME'] = date('YmdHis');
					}
				}
			}
		}
	}
	
	function update_mail()
	{
		$this->C->setting()->get_setting();
		
		foreach($this->D['SETTING']['HELPDESK']['ACCOUNT']['D'] as $kACC => $vACC )
		{
			if($vACC['ACTIVE'])
			{
				$mail = $this->C->mail()->receive([
					'DATETIME' => $this->D['SETTING']['HELPDESK']['ACCOUNT']['D'][$kACC]['IN']['UDATETIME'],
					'CONNECT' => ['METHOD' => 'IMAP', 'SERVER' => $vACC['IN']['SERVER'], 'PORT' => $vACC['IN']['PORT'] , 'USERNAME' => $vACC['IN']['USER'], 'PASSWORD' => $vACC['IN']['PASSWORD']] ]);
				
				#$mail['EMAIL']['D'][ $email ][ $prettydate ];
				
				
				foreach((array)$mail['EMAIL']['D'] as $kMAIL => $vMAIL )
				{
					foreach($vMAIL as $kDATE => $vDATE )
					{
						$mail_domain = explode('@', $vDATE['FROM']);
						if( $mail_domain[1] != '' && ($vACC['IN']['WHITELIST'] == '' || strpos($vACC['IN']['WHITELIST'],'*@'.$mail_domain[1]) !== false || strpos($vACC['IN']['WHITELIST'],$vDATE['FROM']) !== false) 
							&& (strpos($vACC['IN']['BLACKLIST'],'*@'.$mail_domain[1]) === false || strpos($vACC['IN']['BLACKLIST'],$vDATE['FROM']) === false) ) #Filter
						{
							$TID = md5($kMAIL);
							$this->D['HELPDESK']['D']['TICKET']['D'][ $TID ]['MESSAGE']['D'][ $kDATE ] = [
									'WAY'		=> 'in',
									'FROM'		=> $vDATE['FROM'],
									'TO'		=> $vACC['OUT']['EMAIL'],
									'TITLE'		=> $vDATE['SUBJECT'],
									'TEXT'		=> $vDATE['BODY'],
									'DATETIME'	=> $vDATE['DATETIME'],
								];
								
							$this->D['HELPDESK']['D']['TICKET']['D'][ $TID ]['DATETIME'] = $vDATE['DATETIME'];
							$this->D['HELPDESK']['D']['TICKET']['D'][ $TID ]['ACTIVE'] = 1;
							$this->D['HELPDESK']['D']['TICKET']['D'][ $TID ]['GROUP_ID'] = $vACC['GROUP_ID'];
						}
					}
				}
				$this->D['SETTING']['HELPDESK']['ACCOUNT']['D'][$kACC]['IN']['UDATETIME'] = date('YmdHis');
			}
		}
		
		$this->C->setting()->set_setting();
		
		/*$mailserver="pop3.web.de";
		$port="995/pop3/ssl";
		$user="xxxx@web.de";
		$pass="xxxxxx";
		$imap = imap_open( "{" . $mailserver . ":" . $port . "}INBOX", $user, $pass );
		if ($imap)
		 {
			echo "Connected\n";
			$check = imap_mailboxmsginfo($imap);
			echo "Date: "     . $check->Date    . "<br />\n" ;
			echo "Driver: "   . $check->Driver  . "<br />\n" ;
			echo "Unread: "   . $check->Unread  . "<br />\n" ;
			echo "Size: "     . $check->Size    . "<br />\n" ;

			$totalrows = imap_num_msg($imap);
			//iterate through all unread mails
			for ($index = 0; $index < $totalrows; $index++)
			{#echo "<textarea style='width:100%;height:400px;'>";
				$header = imap_header($imap, $index + 1);
				 //get mail subject
				echo("<h1>".$header->subject."</h1>");
				 //get mail sent date
				$prettydate = date(DateTime::ISO8601 , $header->udate);
				echo $header->udate.'|';echo( $prettydate );echo "|<br>";
				//get email author
				$email = "{$header->from[0]->mailbox}@{$header->from[0]->host}";
				echo( $email );echo "|<br>";
				//get mail body
				 # $struktur = imap_fetchstructure ($imap,3); echo "||||";print_r( $struktur);
				##print_R( imap_fetchbody($imap,13,1.2) );
				#echo( imap_body($imap, $index + 1));
				#echo "</textarea>";
				#exit;
				$TEXT = imap_fetchbody($imap,13,1.2)?:imap_fetchbody($imap,13,1);
				$this->D['HELPDESK']['D']['TICKET']['D'][ md5($email) ]['MESSAGE']['D'][ $prettydate ] = [
				'EMAIL'		=> $email,
				'TITLE'		=> $header->subject,
				'DATETIME'	=> $prettydate,
				#'TEXT'	=> $TEXT,
				];
			}
			//close connection to mailbox
			imap_close($imap);
			return true;
		 }
		 else
		 {
			 echo("Can't connect: " . imap_last_error());
			 return false;
		 }
		 */
	}

	function get_ticket()
	{
			$W = $this->C->db()->where_interpreter(array(
			'MESSAGE_TITLE'	=> "id IN (SELECT ticket_id FROM helpdesk_message WHERE title LIKE '%MESSAGE_TITLE%')",
			'MESSAGE_TEXT'	=> "id IN (SELECT ticket_id FROM helpdesk_message WHERE text LIKE '%MESSAGE_TEXT%')",
			'USER_ID'		=> "group_id IN (SELECT group_id FROM helpdesk_group_to_user WHERE user_id IN ('USER_ID') )",
			'ACTIVE'		=> "active IN ('ACTIVE')",
			'GROUP_ID'		=> "group_id IN ('GROUP_ID')",
			'PRIORITY'		=> "priority IN ('PRIORITY')",
		),$this->D['HELPDESK']['D']['TICKET']['W']);
		$L = (strlen($this->D['HELPDESK']['D']['TICKET']['L']['START']) && strlen($this->D['HELPDESK']['D']['TICKET']['L']['STEP']))? " LIMIT {$this->D['HELPDESK']['D']['TICKET']['L']['START']},{$this->D['HELPDESK']['D']['TICKET']['L']['STEP']}":'';
		
		$qry = $this->C->db()->query("SELECT id, group_id, user_id, priority, active, number,datetime, utimestamp,itimestamp FROM helpdesk_ticket WHERE 1 {$W} ORDER BY active, priority DESC,  datetime DESC {$L}");
		while ($ary = $qry->fetch_array(MYSQLI_ASSOC))
		{
			$this->D['HELPDESK']['D']['TICKET']['D'][ $ary['id'] ] = array(
				'USER_ID'		=> $ary['user_id'],
				'ACTIVE'		=> $ary['active'],
				'NUMBER'		=> $ary['number'],
				'GROUP_ID'		=> $ary['group_id'],
				'PRIORITY'		=> $ary['priority'],
				'DATETIME'		=> $ary['datetime'],
				'ITIMESTAMP'	=> $ary['itimestamp'],
				'UTIMESTAMP'	=> $ary['utimestamp'],
			);
		}
		$ary = $this->C->db()->query("SELECT count(*) AS COUNT FROM helpdesk_ticket WHERE 1 {$W} ")->fetch_array(MYSQLI_ASSOC);
		$this->D['HELPDESK']['D']['TICKET']['COUNT'] = $ary['COUNT'];

		#Message
		$k = implode("','",array_keys((array)$this->D['HELPDESK']['D']['TICKET']['D']));
		$qry = $this->C->db()->query("SELECT m.id, user_id, ticket_id, m.active, way, `to`,`from`, title, text, datetime, YEAR(datetime) Y, MONTH(datetime) M, DAY(datetime) D, m.utimestamp, m.itimestamp 
		,u.nickname
		FROM helpdesk_message m 
		LEFT JOIN user_user u ON m.user_id = u.id 
		WHERE ticket_id IN ('{$k}') ORDER BY datetime");
		while ($ary = $qry->fetch_array(MYSQLI_ASSOC))
		{
			$this->D['HELPDESK']['D']['TICKET']['D'][ $ary['ticket_id'] ]['MESSAGE']['D'][ $ary['id'] ] = array(
				'USER_ID'		=> $ary['user_id'],
				'USER'			=> [ 'NICK' => $ary['nickname']  ],
				'ACTIVE'		=> $ary['active'],
				'WAY'			=> $ary['way'],#in, out
				'TO'			=> $ary['to'],
				'FROM'			=> $ary['from'],
				'TITLE'			=> $ary['title'],
				'TEXT'			=> $ary['text'],
				'DATETIME'		=> $ary['datetime'],
				'ITIMESTAMP'	=> $ary['itimestamp'],
				'UTIMESTAMP'	=> $ary['utimestamp'],
			);
			$this->D['HELPDESK']['D']['MESSAGE'][ $ary['way'] ]['YEAR'][$ary['Y']]['COUNT'] ++;
			$this->D['HELPDESK']['D']['MESSAGE'][ $ary['way'] ]['YEAR'][$ary['Y']]['MONTH'][$ary['M']]['COUNT'] ++;
			$this->D['HELPDESK']['D']['MESSAGE'][ $ary['way'] ]['YEAR'][$ary['Y']]['MONTH'][$ary['M']]['DAY'][$ary['D']]['COUNT'] ++;
		}
	}
	
	function get_message()
	{
		$W = $this->C->db()->where_interpreter(array(
			'ID'			=> "m.id IN ('ID')",
			'TICKET_ID'		=> "ticket_id IN ('TICKET_ID')",
			'ACTIVE'		=> "m.active = 'ACTIVE'",
		),$this->D['HELPDESK']['D']['MESSAGE']['W']);
		#$W = ($this->D['HELPDESK']['D']['MESSAGE']['W'])? " AND {$this->D['HELPDESK']['D']['MESSAGE']['W']}":'';
		$qry = $this->C->db()->query("SELECT m.id, m.user_id, m.ticket_id, m.active, way, `to`,`from`, title, text, datetime, m.utimestamp, m.itimestamp
									,u.nickname
									FROM helpdesk_message m
									LEFT JOIN user_user u ON m.user_id = u.id 
									WHERE 1 {$W} ORDER BY datetime");
		while ($ary = $qry->fetch_array(MYSQLI_ASSOC))
		{
			$this->D['HELPDESK']['D']['MESSAGE']['D'][ $ary['id'] ] = array(
				'USER_ID'		=> $ary['user_id'],
				'USER'			=> [ 'NICK' => $ary['nickname']  ],
				'TICKET_ID'		=> $ary['ticket_id'],
				'ACTIVE'		=> $ary['active'],
				'WAY'			=> $ary['way'],
				'TO'			=> $ary['to'],
				'FROM'			=> $ary['from'],
				'TITLE'			=> $ary['title'],
				'TEXT'			=> $ary['text'],
				'DATETIME'		=> $ary['datetime'],
				'ITIMESTAMP'	=> $ary['itimestamp'],
				'UTIMESTAMP'	=> $ary['utimestamp'],
			);
		}
		$keys = implode("','",array_keys((array)$this->D['HELPDESK']['D']['MESSAGE']['D']));
		$qry = $this->C->db()->query("SELECT file_seoname_id, message_id, hmfs.active, hmfs.utimestamp, hmfs.itimestamp,
											dfs.name
										FROM helpdesk_message_file_seoname hmfs, data_file_seoname dfs
										WHERE message_id IN ('{$keys}')");
		while($ary = $qry->fetch_array(MYSQLI_ASSOC))
		{
			$this->D['HELPDESK']['D']['MESSAGE']['D'][ $ary['message_id'] ]['FILE_SEONAME']['D'][ $ary['file_seoname_id'] ] = array(
				'ACTIVE'		=> $ary['active'],
				'NAME'			=> $ary['name'], #seo name
				'ITIMESTAMP'	=> $ary['itimestamp'],
				'UTIMESTAMP'	=> $ary['utimestamp'],
			);
			$fileIDs .= ($fileIDs?"','":'').$ary['file_id'];
		}

	}
	
	function get_textblock()
	{
		$W = ($this->D['HELPDESK']['D']['TEXTBLOCK']['W']['ID'])? " AND id IN ('{$this->D['HELPDESK']['D']['TEXTBLOCK']['W']['ID']}')":'';
		$qry = $this->C->db()->query("SELECT id, active, title, text, utimestamp, itimestamp FROM helpdesk_textblock WHERE 1 {$W} ORDER BY title");
		while ($ary = $qry->fetch_array(MYSQLI_ASSOC))
		{
			$this->D['HELPDESK']['D']['TEXTBLOCK']['D'][ $ary['id'] ] = array(
				'ACTIVE'		=> $ary['active'],
				'TITLE'			=> $ary['title'],
				'TEXT'			=> $ary['text'],
				'ITIMESTAMP'	=> $ary['itimestamp'],
				'UTIMESTAMP'	=> $ary['utimestamp'],
			);
		}
	}
	
	function set_textblock()
	{
		foreach((array)$this->D['HELPDESK']['D']['TEXTBLOCK']['D'] as $k => $v )
		{
			if($v['ACTIVE'] != -1)
			{
				$IU .= (($IU)?',':'')."('{$k}'";
				$IU .= (isset($v['ACTIVE']))?", '{$v['ACTIVE']}'":", NULL";
				$IU .= (isset($v['TITLE']))?", '{$v['TITLE']}'":", NULL";
				$IU .= (isset($v['TEXT']))?", '{$v['TEXT']}'":", NULL";
				$IU .= ")";
			}
			else
				$DEL .= ($DEL?',':'')."'{$k}'";
		}
		
		if($IU)
			$this->C->db()->query("INSERT INTO helpdesk_textblock (id, active, title, text) VALUES {$IU}
									ON DUPLICATE KEY UPDATE
									active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE helpdesk_textblock.active END,
									title = CASE WHEN VALUES(title) IS NOT NULL THEN VALUES(title) ELSE helpdesk_textblock.title END,
									text = CASE WHEN VALUES(text) IS NOT NULL THEN VALUES(text) ELSE helpdesk_textblock.text END
									");
									
		if($DEL)
			$this->C->db()->query("DELETE FROM helpdesk_textblock WHERE id IN ({$DEL})");

	}
	
	function get_group()
	{
		$W = $this->C->db()->where_interpreter(array(
			'USER_ID'		=> "id IN (SELECT group_id FROM helpdesk_group_to_user WHERE user_id = 'USER_ID')",
			'ACTIVE'		=> "active = 'ACTIVE'",
			'GROUP_ID'		=> "group_id = 'GROUP_ID'",
		),$this->D['HELPDESK']['D']['GROUP']['W']);
		
		$qry = $this->C->db()->query("SELECT id, active, title, utimestamp, itimestamp,
		(SELECT count(*) FROM helpdesk_ticket ht WHERE ht.active = 1 AND ht.group_id = hg.id ) AS offen
		FROM helpdesk_group hg WHERE 1 {$W} ORDER BY title");
		while ($ary = $qry->fetch_array(MYSQLI_ASSOC))
		{
			$this->D['HELPDESK']['D']['GROUP']['D'][ $ary['id'] ] = array(
				'ACTIVE'		=> $ary['active'],
				'TITLE'			=> $ary['title'],
				'ITIMESTAMP'	=> $ary['itimestamp'],
				'UTIMESTAMP'	=> $ary['utimestamp'],
			);
			$this->D['HELPDESK']['D']['GROUP']['D'][ $ary['id'] ]['TICKET']['ACTIVE'][1]['COUNT'] = $ary['offen'];
		}
		
		
		#Textblock
		$k = implode("','",array_keys((array)$this->D['HELPDESK']['D']['GROUP']['D']));
		$qry = $this->C->db()->query("SELECT group_id, textblock_id, active, utimestamp, itimestamp FROM helpdesk_group_to_textblock WHERE group_id IN ('{$k}')");
		while ($ary = $qry->fetch_array(MYSQLI_ASSOC))
		{
			$this->D['HELPDESK']['D']['GROUP']['D'][ $ary['group_id'] ]['TEXTBLOCK']['D'][ $ary['textblock_id'] ] = array(
				'ACTIVE'		=> $ary['active'],
				'ITIMESTAMP'	=> $ary['itimestamp'],
				'UTIMESTAMP'	=> $ary['utimestamp'],
			);
		}
		
		#User
		$qry = $this->C->db()->query("SELECT group_id, user_id, active, utimestamp, itimestamp FROM helpdesk_group_to_user WHERE group_id IN ('{$k}')");
		while ($ary = $qry->fetch_array(MYSQLI_ASSOC))
		{
			$this->D['HELPDESK']['D']['GROUP']['D'][ $ary['group_id'] ]['USER']['D'][ $ary['user_id'] ] = array(
				'ACTIVE'		=> $ary['active'],
				'ITIMESTAMP'	=> $ary['itimestamp'],
				'UTIMESTAMP'	=> $ary['utimestamp'],
			);
		}
	}
	
	function set_group()
	{
		foreach((array)$this->D['HELPDESK']['D']['GROUP']['D'] as $k => $v )
		{
			if($v['ACTIVE'] != -1)
			{
				$IU .= (($IU)?',':'')."('{$k}'";
				$IU .= (isset($v['ACTIVE']))?", '{$v['ACTIVE']}'":", NULL";
				$IU .= (isset($v['TITLE']))?", '{$v['TITLE']}'":", NULL";
				$IU .= ")";
				
				foreach((array)$v['TEXTBLOCK']['D'] as $k1 => $v1 )
				{
					if($v1['ACTIVE'] != -1)
					{
						$IU1 .= (($IU1)?',':'')."('{$k}','{$k1}'";
						$IU1 .= (isset($v1['ACTIVE']))?", '{$v1['ACTIVE']}'":", NULL";
						$IU1 .= ")";
					}
					else
						$DEL1 .= ($DEL1?',':'')."'{$k}{$k1}'";
				}
				foreach((array)$v['USER']['D'] as $k1 => $v1 )
				{
					if($v1['ACTIVE'] != -1)
					{
						$IU2 .= (($IU2)?',':'')."('{$k}','{$k1}'";
						$IU2 .= (isset($v1['ACTIVE']))?", '{$v1['ACTIVE']}'":", NULL";
						$IU2 .= ")";
					}
					else
						$DEL2 .= ($DEL2?',':'')."'{$k}{$k1}'";
				}
			}
			else
				$DEL .= ($DEL?',':'')."'{$k}'";
		}
		
		if($IU)
			$this->C->db()->query("INSERT INTO helpdesk_group (id, active, title) VALUES {$IU}
									ON DUPLICATE KEY UPDATE
									active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE helpdesk_group.active END,
									title = CASE WHEN VALUES(title) IS NOT NULL THEN VALUES(title) ELSE helpdesk_group.title END
									");
		if($IU1)
			$this->C->db()->query("INSERT INTO helpdesk_group_to_textblock (group_id, textblock_id, active) VALUES {$IU1}
									ON DUPLICATE KEY UPDATE
									active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE helpdesk_group_to_textblock.active END
									");
		if($IU2)
			$this->C->db()->query("INSERT INTO helpdesk_group_to_user (group_id, user_id, active) VALUES {$IU2}
									ON DUPLICATE KEY UPDATE
									active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE helpdesk_group_to_user.active END
									");
									
		if($DEL)
		{
			$this->C->db()->query("DELETE FROM helpdesk_group WHERE id IN ({$DEL})");
			$this->C->db()->query("DELETE FROM helpdesk_group_to_textblock WHERE CONCAT(group_id) IN ({$DEL})");
			$this->C->db()->query("DELETE FROM helpdesk_group_to_user WHERE CONCAT(group_id) IN ({$DEL})");
		}
		
		if($DEL1)
			$this->C->db()->query("DELETE FROM helpdesk_group_to_textblock WHERE CONCAT(group_id,textblock_id) IN ({$DEL1})");
		if($DEL2)
			$this->C->db()->query("DELETE FROM helpdesk_group_to_user WHERE CONCAT(group_id,user_id) IN ({$DEL2})");
	}
	
	function set_ticket()
	{
		foreach((array)$this->D['HELPDESK']['D']['TICKET']['D'] as $k => $v )
		{
			if($v['ACTIVE'] != -1)
			{
				#$TICKET = $this->D['HELPDESK']['D']['TICKET']['D'][$k];
				$IU .= (($IU)?',':'')."('{$k}'";
				$IU .= (isset($v['GROUP_ID']))?", '{$v['GROUP_ID']}'":", NULL";
				$IU .= (isset($v['USER_ID']))?", '{$v['USER_ID']}'":", NULL";
				$IU .= (isset($v['PRIORITY']))?", '{$v['PRIORITY']}'":", NULL";
				$IU .= (isset($v['ACTIVE']))?", '{$v['ACTIVE']}'":", NULL";
				$IU .= (isset($v['DATETIME']))?", '{$v['DATETIME']}'":", NULL";
				$IU .= ")";
				#Message
				foreach((array)$v['MESSAGE']['D'] as $k1 => $v1 )
				{
					if($v1['ACTIVE'] != -2)
					{
						$IU1 .= (($IU1)?',':'')."('{$k1}','{$k}'";
						$IU1 .= (isset($v1['USER_ID']))?", '{$v1['USER_ID']}'":", NULL";
						$IU1 .= (isset($v1['ACTIVE']))?", '{$v1['ACTIVE']}'":", NULL";
						$IU1 .= (isset($v1['WAY']))?", '{$v1['WAY']}'":", NULL";
						$IU1 .= (isset($v1['TO']))?", '{$v1['TO']}'":", NULL";
						$IU1 .= (isset($v1['FROM']))?", '{$v1['FROM']}'":", NULL";
						$IU1 .= (isset($v1['TITLE']))?", '{$v1['TITLE']}'":", NULL";
						$IU1 .= (isset($v1['TEXT']))?", '".$this->C->db()->real_escape_string($v1['TEXT'])."'":", NULL";
						$IU1 .= (isset($v1['DATETIME']))?", '{$v1['DATETIME']}'":", NULL";
						$IU1 .= ")";
						
						#File D[HELPDESK][D][TICKET][D][{$kTICKET}][MESSAGE][D]["+mid+"][FILE_SEONAME][D]["+seo_id+"][ACTIVE]
						foreach((array)$v1['FILE_SEONAME']['D'] as $k2 => $v2 )
						{
							if($v2['ACTIVE'] != -2)
							{
								$IU2 .= (($IU2)?',':'')."('{$k2}','{$k1}'";
								$IU2 .= (isset($v2['ACTIVE']))?", '{$v2['ACTIVE']}'":", NULL";
								$IU2 .= ")";
							}
							else
							{
								$DEL2 .= ($DEL2?',':'')."'{$k2}{$k1}'";
							}
						}
					}
					else
					{
						$DEL1 .= ($DEL1?',':'')."'{$k}{$k1}'";
					}
				}
				
			}
			else
			{
				$DEL .= ($DEL?',':'')."'{$k}'";
			}
		}
		
		if($IU)
			$this->C->db()->query("INSERT INTO helpdesk_ticket (id, group_id, user_id, priority, active, datetime) VALUES {$IU}
									ON DUPLICATE KEY UPDATE
									group_id = CASE WHEN VALUES(group_id) IS NOT NULL THEN VALUES(group_id) ELSE helpdesk_ticket.group_id END,
									user_id = CASE WHEN VALUES(user_id) IS NOT NULL THEN VALUES(user_id) ELSE helpdesk_ticket.user_id END,
									priority = CASE WHEN VALUES(priority) IS NOT NULL THEN VALUES(priority) ELSE helpdesk_ticket.priority END,
									active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE helpdesk_ticket.active END,
									datetime = CASE WHEN VALUES(datetime) IS NOT NULL THEN VALUES(datetime) ELSE helpdesk_ticket.datetime END
									");
		if($IU1)
			$this->C->db()->query("INSERT INTO helpdesk_message (id, ticket_id, user_id, active, way,`to`,`from`, title, text, datetime) VALUES {$IU1}
									ON DUPLICATE KEY UPDATE
									ticket_id = CASE WHEN VALUES(ticket_id) IS NOT NULL THEN VALUES(ticket_id) ELSE helpdesk_message.ticket_id END,
									active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE helpdesk_message.active END,
									way = CASE WHEN VALUES(way) IS NOT NULL THEN VALUES(way) ELSE helpdesk_message.way END,
									`to` = CASE WHEN VALUES(`to`) IS NOT NULL THEN VALUES(`to`) ELSE helpdesk_message.`to` END,
									`from` = CASE WHEN VALUES(`from`) IS NOT NULL THEN VALUES(`from`) ELSE helpdesk_message.`from` END,
									title = CASE WHEN VALUES(title) IS NOT NULL THEN VALUES(title) ELSE helpdesk_message.title END,
									text = CASE WHEN VALUES(text) IS NOT NULL THEN VALUES(text) ELSE helpdesk_message.text END,
									datetime = CASE WHEN VALUES(datetime) IS NOT NULL THEN VALUES(datetime) ELSE helpdesk_message.datetime END
									");
		if($IU2)
			$this->C->db()->query("INSERT INTO helpdesk_message_file_seoname (file_seoname_id, message_id, active) VALUES {$IU2}
									ON DUPLICATE KEY UPDATE
									active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE helpdesk_message_file_seoname.active END
									");
		if($DEL)
		{
			$this->C->db()->query("DELETE FROM helpdesk_ticket WHERE id IN ({$DEL})");
			$this->C->db()->query("DELETE FROM helpdesk_message WHERE CONCAT(ticket_id) IN ({$DEL})");
			$this->C->db()->query("DELETE FROM helpdesk_message_file_seoname WHERE message_id NOT IN (SELECT id FROM helpdesk_message)");
		}
		
		if($DEL1)
			$this->C->db()->query("DELETE FROM helpdesk_message WHERE CONCAT(id,ticket_id) IN ({$DEL1})");
		
		if($DEL2)
			$this->C->db()->query("DELETE FROM helpdesk_message_file_seoname WHERE CONCAT(file_seoname_id, message_id) IN ({$DEL2})");
	}
    

} 