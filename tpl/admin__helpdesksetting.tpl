{function name='add_priority'}
	{foreach from=$D.SETTING.HELPDESK.D.PRIORITY.D name=STA item=STA key=kSTA}
	<tr id='STATUS_{$kSTA}'>
		<td><button class="btn btn-danger btn-xs" type="button" onclick="document.getElementById('D[SETTING][HELPDESK][D][STATUS][D][{$kSTA}][ACTIVE]').value = '-2';document.getElementById('STATUS_{$kSTA}').style.display = 'none';"><i class="fa fa-trash" aria-hidden="true"></i></button></td>
		<td>
			<input id="D[SETTING][HELPDESK][D][STATUS][D][{$kSTA}][ACTIVE]" name="D[SETTING][HELPDESK][D][PRIORITY][D][{$kSTA}][ACTIVE]" type="hidden" value="{$STA.ACTIVE}">
			<input type="checkbox" onclick="document.getElementById('D[SETTING][HELPDESK][D][PRIORITY][D][{$kSTA}][ACTIVE]').value = this.checked?1:0;" {if $STA.ACTIVE}checked{/if}>
		</td>
		<td>
			<div class="input-group">
				<span class="input-group-addon" style="min-width:100px;">Color</span>
				<input class="form-control" name="D[SETTING][HELPDESK][D][PRIORITY][D][{$kSTA}][COLOR]" value="{$STA.COLOR}">
			</div>
		</td>
		<td>
			<div class="input-group">
				<span class="input-group-addon" style="min-width:100px;">Title</span>
				<input class="form-control" name="D[SETTING][HELPDESK][D][PRIORITY][D][{$kSTA}][TITLE]" value="{$STA.TITLE}">
			</div>
		</td>
	</tr>
	{/foreach}
{/function}
{function name='add_status'}
	{foreach from=$D.SETTING.HELPDESK.D.STATUS.D name=STA item=STA key=kSTA}
	<tr id='STATUS_{$kSTA}'>
		<td><button class="btn btn-danger btn-xs" type="button" onclick="document.getElementById('D[SETTING][HELPDESK][D][STATUS][D][{$kSTA}][ACTIVE]').value = '-2';document.getElementById('STATUS_{$kSTA}').style.display = 'none';"><i class="fa fa-trash" aria-hidden="true"></i></button></td>
		<td>
			<input id="D[SETTING][HELPDESK][D][STATUS][D][{$kSTA}][ACTIVE]" name="D[SETTING][HELPDESK][D][STATUS][D][{$kSTA}][ACTIVE]" type="hidden" value="{$STA.ACTIVE}">
			<input type="checkbox" onclick="document.getElementById('D[SETTING][HELPDESK][D][STATUS][D][{$kSTA}][ACTIVE]').value = this.checked?1:0;" {if $STA.ACTIVE}checked{/if}>
		</td>
		<td>
			<div class="input-group">
				<span class="input-group-addon" style="min-width:100px;">Color</span>
				<input class="form-control" name="D[SETTING][HELPDESK][D][STATUS][D][{$kSTA}][COLOR]" value="{$STA.COLOR}">
			</div>
		</td>
		<td>
			<div class="input-group">
				<span class="input-group-addon" style="min-width:100px;">Title</span>
				<input class="form-control" name="D[SETTING][HELPDESK][D][STATUS][D][{$kSTA}][TITLE]" value="{$STA.TITLE}">
			</div>
		</td>
	</tr>
	{/foreach}
{/function}
{function name='add_textblock'}
	{foreach from=$D.HELPDESK.D.TEXTBLOCK.D name=TEX item=TEX key=kTEX}
	<tr id='TEXTBLOCK_{$kTEX}'>
		<td><button class="btn btn-danger btn-xs" type="button" onclick="document.getElementById('D[HELPDESK][D][TEXTBLOCK][D][{$kTEX}][ACTIVE]').value = '-1';document.getElementById('TEXTBLOCK_{$kTEX}').style.display = 'none';"><i class="fa fa-trash" aria-hidden="true"></i></button></td>
		<td>
			<input id="D[HELPDESK][D][TEXTBLOCK][D][{$kTEX}][ACTIVE]" name="D[HELPDESK][D][TEXTBLOCK][D][{$kTEX}][ACTIVE]" type="hidden" value="{$TEX.ACTIVE}">
			<input type="checkbox" onclick="document.getElementById('D[HELPDESK][D][TEXTBLOCK][D][{$kTEX}][ACTIVE]').value = this.checked?1:0;" {if $TEX.ACTIVE}checked{/if}>
			</td>
		<td>
			<div class="input-group">
				<span class="input-group-addon" style="min-width:100px;">Title</span>
				<input class="form-control" name="D[HELPDESK][D][TEXTBLOCK][D][{$kTEX}][TITLE]" value="{$TEX.TITLE}">
			</div>
			<div class="input-group">
				<span class="input-group-addon" style="min-width:100px;">Text</span>
				<textarea class="form-control" name="D[HELPDESK][D][TEXTBLOCK][D][{$kTEX}][TEXT]">{$TEX.TEXT}</textarea>
			</div>
		</td>
		<td>
			<ul>
			{foreach from=$D.HELPDESK.D.GROUP.D name=GROUP item=GROUP key=kGROUP}
				<li><input type="checkbox" {if $GROUP.TEXTBLOCK.D[{$kTEX}].ACTIVE}checked{/if} name="D[HELPDESK][D][GROUP][D][{$kGROUP}][TEXTBLOCK][D][{$kTEX}][ACTIVE]" value="1">{$GROUP.TITLE}</li>
			{/foreach}
			</ul>
		</td>
	</tr>
	{/foreach}
{/function}
{function name='add_group'}
	{foreach from=$D.HELPDESK.D.GROUP.D name=GROUP item=GROUP key=kGROUP}
		<tr id='GROUP_{$kGROUP}'>
			<td><button class="btn btn-danger btn-xs" type="button" onclick="document.getElementById('D[HELPDESK][D][GROUP][D][{$kGROUP}][ACTIVE]').value = '-1';document.getElementById('GROUP_{$kGROUP}').style.display = 'none';"><i class="fa fa-trash" aria-hidden="true"></i></button></td>
			<td><input id="D[HELPDESK][D][GROUP][D][{$kGROUP}][ACTIVE]" name="D[HELPDESK][D][GROUP][D][{$kGROUP}][ACTIVE]" type="hidden" value="{$GROUP.ACTIVE}">
				<input type="checkbox" onclick="document.getElementById('D[HELPDESK][D][GROUP][D][{$kGROUP}][ACTIVE]').value = this.checked?1:0;" {if $GROUP.ACTIVE}checked{/if}>
			</td>
			<td><input class="form-control" name="D[HELPDESK][D][GROUP][D][{$kGROUP}][TITLE]" value="{$GROUP.TITLE}"></td>
			<td>
				<ul>
				{foreach from=$D.USER.D name=USE item=USE key=kUSE}
					<li>
						<input type="checkbox" {if $GROUP.USER.D[$kUSE].ACTIVE}checked{/if} onclick="document.getElementById('D[HELPDESK][D][GROUP][D][{$kGROUP}][USER][D][{$kUSE}][ACTIVE]').value = (this.checked)?1:-1">
						<input type='hidden' id="D[HELPDESK][D][GROUP][D][{$kGROUP}][USER][D][{$kUSE}][ACTIVE]" name="D[HELPDESK][D][GROUP][D][{$kGROUP}][USER][D][{$kUSE}][ACTIVE]" value="{$GROUP.USER.D[$kUSE].ACTIVE}">
						{$D.USER.D[$kUSE].NICK}
					</li>
				{/foreach}
				</ul>
			</td>
		</tr>
	{/foreach}
{/function}
{function name='add_emailbox'}
	{foreach from=$D.SETTING.HELPDESK.ACCOUNT.D name=ACC item=ACC key=kACC}
	<tr id='ACCOUNT_{$kACC}'>
		<td><button class="btn btn-danger btn-xs" type="button" onclick="document.getElementById('D[SETTING][HELPDESK][ACCOUNT][D][{$kACC}][ACTIVE]').value = '-2';document.getElementById('ACCOUNT_{$kACC}').style.display = 'none';"><i class="fa fa-trash" aria-hidden="true"></i></button></td>
		<td>
			<label for="basic-url">Account ID: {$kACC}</label>
			<div class="input-group">
				<span class="input-group-addon" style="min-width:100px;">Active</span>
				<input class="form-control" id="D[SETTING][HELPDESK][ACCOUNT][D][{$kACC}][ACTIVE]" name="D[SETTING][HELPDESK][ACCOUNT][D][{$kACC}][ACTIVE]" value="{$ACC.ACTIVE}">
			</div>
			<div class="input-group">
				<span class="input-group-addon" style="min-width:100px;">Title</span>
				<input class="form-control" name="D[SETTING][HELPDESK][ACCOUNT][D][{$kACC}][TITLE]" value="{$ACC.TITLE}">
			</div>
			<div class="input-group">
				<span class="input-group-addon" style="min-width:100px;">Gruppe</span>
				<select class="form-control" name="D[SETTING][HELPDESK][ACCOUNT][D][{$kACC}][GROUP_ID]">
					{foreach from=$D.HELPDESK.D.GROUP.D name=GRO item=GRO key=kGRO}
					<option value="{$kGRO}" {if $kGRO == $ACC.GROUP_ID}selected{/if}>{$GRO.TITLE}</option>
					{/foreach}
				</select>
			</div>
			<label for="basic-url">last Eingang Update: {$ACC.IN.UDATETIME}</label>
		</td>
		<td>
		
		<div class="tab-content">
			<ul class="nav nav-tabs" role="tablist">
				<li class="active"><a href="#{$kACC}_0" data-toggle="tab">Einstellungen</a></li>
				<li><a href="#{$kACC}_1" data-toggle="tab">White-List</a></li>
				<li><a href="#{$kACC}_2" data-toggle="tab">Black-List</a></li>
			</ul>
			<div class="panel-body tab-pane active" id="{$kACC}_0">
			
				<div class="input-group">
					<span class="input-group-addon" style="min-width:100px;">Server</span>
					<input class="form-control" name="D[SETTING][HELPDESK][ACCOUNT][D][{$kACC}][IN][SERVER]" value="{$ACC.IN.SERVER}">
				</div>
				<div class="input-group">
					<span class="input-group-addon" style="min-width:100px;">Port</span>
					<input class="form-control" name="D[SETTING][HELPDESK][ACCOUNT][D][{$kACC}][IN][PORT]" value="{$ACC.IN.PORT}">
				</div>
				<div class="input-group">
					<span class="input-group-addon" style="min-width:100px;">User</span>
					<input class="form-control" name="D[SETTING][HELPDESK][ACCOUNT][D][{$kACC}][IN][USER]" value="{$ACC.IN.USER}">
				</div>
				<div class="input-group">
					<span class="input-group-addon" style="min-width:100px;">Password</span>
					<input class="form-control" name="D[SETTING][HELPDESK][ACCOUNT][D][{$kACC}][IN][PASSWORD]" value="{$ACC.IN.PASSWORD}">
				</div>
			
			</div>
			<div class="panel-body tab-pane" id="{$kACC}_1">
				<textarea class="form-control" data-toggle="tooltip" data-placement="bottom" title="Erlaubt E-Mails: z.B: *@test.de [enter] test@test.de" name="D[SETTING][HELPDESK][ACCOUNT][D][{$kACC}][IN][WHITELIST]">{$ACC.IN.WHITELIST}</textarea>
				
			</div>
			<div class="panel-body tab-pane" id="{$kACC}_2">
				<textarea class="form-control" style="height:100%;" data-toggle="tooltip" data-placement="bottom" title="Blokiert E-Mails: z.B: *@test.de [enter] test@test.de" name="D[SETTING][HELPDESK][ACCOUNT][D][{$kACC}][IN][BLACKLIST]">{$ACC.IN.BLACKLIST}</textarea>
			</div>
		</div>
		
		</td>
		<td>
		<div class="input-group">
			<span class="input-group-addon" style="min-width:100px;">E-Mail</span>
			<input class="form-control" name="D[SETTING][HELPDESK][ACCOUNT][D][{$kACC}][OUT][EMAIL]" value="{$ACC.OUT.EMAIL}">
		</div>
		<div class="input-group">
			<span class="input-group-addon" style="min-width:100px;">Server</span>
			<input class="form-control" name="D[SETTING][HELPDESK][ACCOUNT][D][{$kACC}][OUT][SERVER]" value="{$ACC.OUT.SERVER}">
		</div>
		<div class="input-group">
			<span class="input-group-addon" style="min-width:100px;">User</span>
			<input class="form-control" name="D[SETTING][HELPDESK][ACCOUNT][D][{$kACC}][OUT][USER]" value="{$ACC.OUT.USER}">
		</div>
		<div class="input-group">
			<span class="input-group-addon" style="min-width:100px;">Password</span>
			<input class="form-control" name="D[SETTING][HELPDESK][ACCOUNT][D][{$kACC}][OUT][PASSWORD]" value="{$ACC.OUT.PASSWORD}">
		</div>
	
		</td>
	</tr>
	{/foreach}
{/function}

{if $D.ACTION == 'add_emailbox'}
	{add_emailbox D=$D}
{elseif $D.ACTION == 'add_group'}
	{add_group D=$D}
{elseif $D.ACTION == 'add_textblock'}
	{add_textblock D=$D}
{elseif $D.ACTION == 'add_status'}
	{add_status D=$D}
{elseif $D.ACTION == 'add_priority'}
	{add_priority D=$D}
{else}
{*switch $D.ACTION}
	{case 'add_emailbox'}
		{add_emailbox D=$D}
	{/case}
	{default}
{/switch*}
	<form id="form">
		<div class="tab-content">
			
				<ul class="nav nav-tabs" role="tablist">
					<li class="active"><a href="#0" data-toggle="tab">Cronjob</a></li>
					<li><a href="#1" data-toggle="tab">Postfach</a></li>
					<li><a href="#2" data-toggle="tab">Gruppen</a></li>
					<li><a href="#3" data-toggle="tab">Text-Bausteine</a></li>
					<!--<li><a href="#4" data-toggle="tab">Einstellungen</a></li>-->
				</ul>
			
			<div class="panel-body tab-pane active" id="0">
				<label for="basic-url">Cronjob</label>
				<br>?D[PAGE]=admin__helpdeskcronjob
				{*<div class="input-group">
					<span class="input-group-addon">Aktive</span>
					<select class="form-control" name="D[SETTING][CRONJOB][D][HELPDESK][ACTIVE]">
						<option value="1" {if $D.SETTING.CRONJOB.D.HELPDESK.ACTIVE == 1}selected{/if}>aktive</option>
						<option vlaue="0" {if $D.SETTING.CRONJOB.D.HELPDESK.ACTIVE != 1}selected{/if}>inaktive</option>
					</select>
				</div>
				
				<div class="input-group">
					<span class="input-group-addon">je</span>
					<input class="form-control" name="D[SETTING][CRONJOB][D][HELPDESK][INTERVAL]" value="{$D.SETTING.CRONJOB.D.HELPDESK.INTERVAL}">
					<span class="input-group-addon">Min</span>
					<input class="form-control" name="D[SETTING][CRONJOB][D][HELPDESK][URL]" value="?D[PAGE]=admin__helpdeskcronjob">
				</div>*}
			</div>
			<div class="panel-body tab-pane" id="1">
				
				<table id="add_emailbox" class="table">
					<thead>
						<th><button type="button" onclick="$.ajax({ url: '?D[PAGE]=admin__helpdesksetting&D[ACTION]=add_emailbox&D[SETTING][HELPDESK][ACCOUNT][D]['+((new Date).getTime())+'][ACTIVE]=1', async: false,success : function(text){ $('#add_emailbox tbody').append(text);}}); return false;" class="btn btn-success btn-xs"><i class="fa fa-plus" aria-hidden="true"></i></button></th>
						<th>Postfach</th>
						<th>E-Mail Eingang</th>
						<th>E-Mail Ausgang</th>
					</thead>
					<tbody>

					{add_emailbox D=$D}					
						
					</tbody>
				</table>
			</div>
			<div class="panel-body tab-pane" id="2">
				
				<table id="add_group" class="table">
					<thead>
						<th><button type="button" onclick="$.ajax({ url: '?D[PAGE]=admin__helpdesksetting&D[ACTION]=add_group&D[HELPDESK][D][GROUP][D]['+((new Date).getTime())+'][ACTIVE]=1', async: false,success : function(text){ $('#add_group tbody').append(text);}}); return false;" class="btn btn-success btn-xs"><i class="fa fa-plus" aria-hidden="true"></i></button></th>
						<th>Active</th>
						<th>title</th>
						<th>User</th>
					</thead>
					<tbody>
					{add_group D=$D}
					</tbody>
				</table>
			</div>
			<div class="panel-body tab-pane" id="3">
				<table id="add_textblock" class="table">
					<thead>
						<th><button type="button" onclick="$.ajax({ url: '?D[PAGE]=admin__helpdesksetting&D[ACTION]=add_textblock&D[HELPDESK][D][TEXTBLOCK][D]['+((new Date).getTime())+'][ACTIVE]=1', async: false,success : function(text){ $('#add_textblock tbody').append(text);}}); return false;" class="btn btn-success btn-xs"><i class="fa fa-plus" aria-hidden="true"></i></button></th>
						<th>Active</th>
						<th>Title / Text</th>
						<th>Gruppe</th>
					</thead>
					<tbody>
					{add_textblock D=$D}
					</tbody>
				</table>
			</div>

			<div class="panel-body tab-pane" id="4">
				Status:
				<table id="add_status" class="table">
					<thead>
						<th><button type="button" onclick="$.ajax({ url: '?D[PAGE]=admin__helpdesksetting&D[ACTION]=add_status&D[SETTING][HELPDESK][D][STATUS][D]['+((new Date).getTime())+'][ACTIVE]=1', async: false,success : function(text){ $('#add_status tbody').append(text);}}); return false;" class="btn btn-success btn-xs"><i class="fa fa-plus" aria-hidden="true"></i></button></th>
						<th>Active</th>
						<th>Title</th>
					</thead>
					<tbody>
					{add_status D=$D}
					</tbody>
				</table>
				Priorität:
				<table id="add_priority" class="table">
					<thead>
						<th><button type="button" onclick="$.ajax({ url: '?D[PAGE]=admin__helpdesksetting&D[ACTION]=add_priority&D[SETTING][HELPDESK][D][PRIORITY][D]['+((new Date).getTime())+'][ACTIVE]=1', async: false,success : function(text){ $('#add_priority tbody').append(text);}}); return false;" class="btn btn-success btn-xs"><i class="fa fa-plus" aria-hidden="true"></i></button></th>
						<th>Active</th>
						<th>Title</th>
					</thead>
					<tbody>
					{add_priority D=$D}
					</tbody>
				</table>
			</div>
			
			<div class="panel-footer text-right">
				<div class="btn-group">
					<button type="button" onclick="$.ajax({ type: 'POST', dataType : 'json', url : '?D[PAGE]=admin__helpdesksetting&D[ACTION]=set_setting', data : $('#form').serialize() });" class="btn btn-default">Speichern</button>
					<button type="button" onclick="$('#page').load('?D[PAGE]=admin__helpdesksetting'); return false;" class="btn btn-default">Abbrechen</button>
				</div>
			</div>
		</div>
	</form>
{/if}