<section class="content">
	<form id="form">
		<div class="tab-content">
			
				<ul class="nav nav-tabs" role="tablist">
					{foreach from=$D.HELPDESK.D.MESSAGE['in'].YEAR name=Y item=Y key=kY}
					<li class="{if $kY == ($smarty.now|date_format:"Y")}active{/if}"><a href="#{$kY}" data-toggle="tab">{$kY}</a></li>
					{/foreach}
				</ul>
			
			
		
	
		{foreach from=$D.HELPDESK.D.MESSAGE['in'].YEAR name=Y item=Y key=kY}
			<div class="panel-body tab-pane {if $kY == ($smarty.now|date_format:"Y")}active{/if}" id="{$kY}">
			
				Nachrichten Statistik

					<table class="table table-hover">
						<tr class="text-right">
							<td>Monat/Tage</td>
							{section name=mon loop=12}
							<td>{$smarty.section.mon.iteration}</td>
							{/section}
						</tr>
					
						{section name=day loop=31} 
							<tr class="text-right">
								<td>{$smarty.section.day.iteration}</td>
								{section name=mon loop=12} 
								<td style="width:7%">{$D.HELPDESK.D.MESSAGE['in'].YEAR[{$kY}].MONTH[{$smarty.section.mon.iteration}].DAY[{$smarty.section.day.iteration}].COUNT}</td>
								{/section}
							</tr>
						{/section}
						<tr class="text-right">
							<td><b>Summe:</b></td>
							{section name=mon loop=12}
							<td><b>{$D.HELPDESK.D.MESSAGE['in'].YEAR[{$kY}].MONTH[{$smarty.section.mon.iteration}].COUNT}</b></td>
							{/section}
						</tr>
					</table>
				
			
			</div>
		{/foreach}
		</div>
	</form>
</section>