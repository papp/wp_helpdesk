{block name="document"}
{function name="get_ticket"}
<div class="panel {if $TICKET.ACTIVE}panel-info{else}panel-default{/if}">
	<div class="panel-heading">
		{*for $i=0 to 4}
			<input type="radio" {if $i==$TICKET.PRIORITY}checked{/if} value="{$i}" name="D[HELPDESK][D][TICKET][D][{$kTICKET}][PRIORITY]">
		{/for*}
		{*if $TICKET.ACTIVE}<i class="fa fa-envelope-open-o"></i>{else}<i class="fa fa-envelope-o"></i>{/if*}
		[#{$TICKET.NUMBER}#]
		| {$TICKET.DATETIME|date_format:"%d.%m.%Y %H:%M"}
		| {$first = current((array)$TICKET.MESSAGE.D)}{$first['TITLE']|wordwrap:50:" ":true|truncate:70:"...":false}
		<div class="pull-right">
			
			<div class="input-group input-group-sm" style="max-width:300px;">
			
				<div class="input-group-btn">
					<button class="btn btn-default" onclick="show_form('{$kTICKET}','');" type="button" title="Notiz"><i class="fa fa-commenting-o"></i></button>
					{if $TICKET.ACTIVE}
						<button title="schließen" class="btn btn-default" onclick="$.ajax({ type: 'POST', dataType : 'json', url : '?D[PAGE]=admin__helpdesklist&D[ACTION]=set_ticket&D[HELPDESK][D][TICKET][D][{$kTICKET}][ACTIVE]=0&D[HELPDESK][D][TICKET][D][{$kTICKET}][GROUP_ID]={$TICKET.GROUP_ID}'} );" type="button"><i class="fa fa-envelope-o"></i></button>
					{else}
						<button title="öffnen" class="btn btn-default" onclick="$.ajax({ type: 'POST', dataType : 'json', url : '?D[PAGE]=admin__helpdesklist&D[ACTION]=set_ticket&D[HELPDESK][D][TICKET][D][{$kTICKET}][ACTIVE]=1&D[HELPDESK][D][TICKET][D][{$kTICKET}][GROUP_ID]={$TICKET.GROUP_ID}'} );" type="button"><i class="fa fa-envelope-open-o"></i></button>
					{/if}
					
				</div>
				
				<select title="Gruppe" class="form-control" onchange="$.ajax({ type: 'POST', dataType : 'json', url : '?D[PAGE]=admin__helpdesklist&D[ACTION]=set_ticket&D[HELPDESK][D][TICKET][D][{$kTICKET}][GROUP_ID]='+this.value} );" name="D[HELPDESK][D][TICKET][D][{$kTICKET}][GROUP_ID]">
					<option value="">---</option>
					{foreach from=$D.HELPDESK.D.GROUP.D name=GROUP item=GROUP key=kGROUP}
						{if $GROUP.ACTIVE == 1}
							<option value="{$kGROUP}" {if $TICKET.GROUP_ID == $kGROUP}selected{/if}>{$GROUP.TITLE}</option>
						{/if}
					{/foreach}
				</select>
				<span class="input-group-addon"></span>
				<select title="Gruppe" class="form-control" onchange="$.ajax({ type: 'POST', dataType : 'json', url : '?D[PAGE]=admin__helpdesklist&D[ACTION]=set_ticket&D[HELPDESK][D][TICKET][D][{$kTICKET}][USER_ID]='+this.value} );" name="D[HELPDESK][D][TICKET][D][{$kTICKET}][USER_ID]">
					<option value="">---</option>
					{foreach from=$D.USER.D name=USR item=USR key=kUSR}
						{if $USR.ACTIVE == 1}
							<option value="{$kUSR}" {if $TICKET.USER_ID == $kUSR}selected{/if}>{$USR.NICK}</option>
						{/if}
					{/foreach}
				</select>
			</div>
			
		</div>
		<div style="clear:both;"></div>
	</div>
	
	{*<div class="row">
	
		<div class="col-xs-3">
			
			<div class="list-group" style="max-height:300px;overflow-y:auto;">
				<table class="table">
					{foreach from=$D.HELPDESK.D.TICKET.D[$kTICKET].MESSAGE.D name=MES item=MES key=kMES}
					<tr>
						<td onclick="$('#frame{$kTICKET}').show();{if $MES.WAY == 'in'}show_form('{$kTICKET}','{$MES.FROM}');{else}$('#frame_form_{$kTICKET}').hide();{/if} document.getElementById('iframe{$kTICKET}').src='?D[PAGE]=admin__helpdesklist&D[ACTION]=get_message&D[HELPDESK][D][MESSAGE][W][ID]={$kMES}&D[HELPDESK][D][MESSAGE][W][TICKET_ID]={$kTICKET}';$('#fl_list{$kTICKET}').empty();">
						<i class="fa {if $MES.WAY == 'in'}fa-chevron-down{elseif $MES.WAY == 'out'}fa-chevron-up{else}fa-commenting-o{/if}"></i> 
					{$MES.ITIMESTAMP|date_format:"%d.%m.%Y %H:%M"}<br>
					{if $MES.USER_ID && ($MES.WAY == 'in' || $MES.WAY == NULL)}<i class="fa fa-user" title="{$MES.USER.NICK}"></i> {$MES.USER.NICK|truncate:4:"":false}{/if} {$MES.FROM}
					
						</td>
						<td>{if $MES.WAY == NULL && $MES.USER.ID == $D.SESSION.USER_ID}<i class="fa fa-trash-o"></i>{/if}</td>
					</tr>
					{/foreach}
				</table>
			</div>
		</div>
		<div class="col-xs-9">*}

			<ul class="list-group">
				<li class="list-group-item no-padding">
					<div class="list-auto-scroll-down" style="clear:both;max-height:200px;overflow-y:scroll;">
						<table class="table table-hover no-margin">
							{foreach from=$D.HELPDESK.D.TICKET.D[$kTICKET].MESSAGE.D name=MES item=MES key=kMES}
							<tr style="cursor:pointer;" class="{if !$MES.TO}list-group-item-warning{/if}" onclick="$('#frame{$kTICKET}').show();{if $MES.WAY == 'in'}show_form('{$kTICKET}','{$MES.FROM}');{else}$('#frame_form_{$kTICKET}').hide();{/if} document.getElementById('iframe{$kTICKET}').src='?D[PAGE]=admin__helpdesklist&D[ACTION]=get_message&D[HELPDESK][D][MESSAGE][W][ID]={$kMES}&D[HELPDESK][D][MESSAGE][W][TICKET_ID]={$kTICKET}';$('#fl_list{$kTICKET}').empty();">
								<td>{$smarty.foreach.MES.iteration}</td>
								<td><i class="fa {if $MES.WAY == 'in'}fa-chevron-down{elseif $MES.WAY == 'out'}fa-chevron-up{else}fa-commenting-o{/if}"></i></td>
								<td>{if $MES.USER_ID && $MES.WAY == 'out'}<i class="fa fa-user" title="{$MES.USER.NICK}"></i> {$MES.USER.NICK|truncate:4:"":false}{/if}</td>
								<td>{$MES.FROM}</td>
								<td>{$MES.TITLE|wordwrap:50:" ":true|truncate:100:"...":false}</td>
								<td style="width:150px;">{$MES.DATETIME|date_format:"%d.%m.%Y %H:%M"}</td>
							</tr>
							{$TITLE = $MES.TITLE}
							{*if $MES.FROM AND $MES.WAY == 'in'}{$FROM = $MES.FROM}{/if*}
							{if $MES.TO AND $MES.WAY == 'in'}{$TO = $MES.TO}{/if}
							{/foreach}
						</table>
					</div>
			
				</li>
				<li class="list-group-item no-padding" id="frame{$kTICKET}" style="display:none;">
					<button type="botton" class="btn btn-xs pull-right" onclick="$('#frame{$kTICKET}').hide();$('#frame_form_{$kTICKET}').hide()" style="position:absolute;right:0px;"><i class="fa fa-times"></i></button>
					<iframe id="iframe{$kTICKET}" style="width:100%;height:300px;border:none;" src=""></iframe>
				</li>
				<li class="list-group-item no-padding" id="frame_form_{$kTICKET}" style="display:none;">
				
					{$MID = $smarty.now} 
					<form id="form{$kTICKET}" method="post" enctype="multipart/form-data">
						
							{*<input type="hidden" name="D[HELPDESK][D][TICKET][D][{$kTICKET}][GROUP_ID]" value="{$TICKET.GROUP_ID}">*}
							<div class="input-group" id="from_to_{$kTICKET}">
							
								<span style="min-width:100px;" class="input-group-addon">
									
								</span>
								<span style="min-width:100px;" class="input-group-addon">Von</span>
								<select name="D[HELPDESK][D][TICKET][D][{$kTICKET}][MESSAGE][D][{$MID}][FROM_ID]" class="form-control">
								{foreach from=$D.SETTING.HELPDESK.ACCOUNT.D name=ACC item=ACC key=kACC}
									{if $ACC.ACTIVE == 1}
									<option value="{$kACC}" {if $ACC.OUT.EMAIL == $TO}selected{/if}>{$ACC.TITLE} [{$ACC.OUT.EMAIL}]</option>
									{/if}
								{/foreach}
								</select>
								
								<span style="min-width:100px;" class="input-group-addon">An</span>
								<input id="D[HELPDESK][D][TICKET][D][{$kTICKET}][TO]" name="D[HELPDESK][D][TICKET][D][{$kTICKET}][MESSAGE][D][{$MID}][TO]" class="form-control" value="">
							</div>
							<div class="input-group">
								<span style="min-width:100px;" class="input-group-addon">Betreff</span>
								<input name="D[HELPDESK][D][TICKET][D][{$kTICKET}][MESSAGE][D][{$MID}][TITLE]" class="form-control" value="[#{$TICKET.NUMBER}#]RE: Antwort">
							</div>
							{if $D.HELPDESK.D.GROUP.D[{$TICKET.GROUP_ID}].TEXTBLOCK.D}
							<div class="input-group">
								<span style="min-width:100px;" class="input-group-addon">Textbaustein</span>
								<select class="form-control" onchange="$.ajax({ url:'?D[PAGE]=admin__helpdesklist&D[ACTION]=get_textblock&D[HELPDESK][D][TEXTBLOCK][W][ID]='+this.value, data:'data', success: function(data){ $('#ta{$kTICKET}{$MID}').val(data); }}) ;//('#ta{$kTICKET}{$MID}').load('?D[PAGE]=helpdesklist&D[ACTION]=get_textblock&D[HELPDESK][D][TEXTBLOCK][W][ID]={$kTEX}');return false;">
									<option value='-'>Text Baustein</option>
									{foreach from=$D.HELPDESK.D.GROUP.D[{$TICKET.GROUP_ID}].TEXTBLOCK.D name=TEX item=TEX key=kTEX}
										{if $TEX.ACTIVE == 1}
										<option value="{$kTEX}">{$D.HELPDESK.D.TEXTBLOCK.D[{$kTEX}].TITLE}</option>
										{/if}
									{/foreach}
								</select>
							</div>
							{/if}
							<div class="input-group">
								<span style="min-width:100px;" class="input-group-addon">Text</span>
								<textarea id="ta{$kTICKET}{$MID}" name="D[HELPDESK][D][TICKET][D][{$kTICKET}][MESSAGE][D][{$MID}][TEXT]" class="form-control" style="height:150px;"></textarea>
							</div>
							<div class="input-group">
								<span class="input-group-btn">
								<button style="min-width:100px;" class="btn btn-default" onclick="$('#fileinput{$kTICKET}').trigger('click');" type="button"><i class="fa fa-plus"></i> Anlage</button>
								</span>
								<div style="height:auto;">
									<ul style="list-style:none;padding:0px;" id="fl_list{$kTICKET}"></ul>
								</div>
							</div>
							
						<div class="panel-footer">
							<div class="btn-group">
								<button class="btn btn-default" onclick="$.ajax({ type: 'POST', dataType : 'json', url : '?D[PAGE]=admin__helpdesklist&D[ACTION]=send_mail', data : $('#form{$kTICKET}').serialize()} );" type="button"><i class="fa fa-envelope-o"></i> Senden</button>
							</div>
							
							
							<script>
							//https://www.html5rocks.com/de/tutorials/file/dndfiles/
							handleFileSelect{$MID} = function(evt){
								//files = evt.target.files;
							
								var data = new FormData();
								jQuery.each(jQuery('#fileinput{$kTICKET}')[0].files, function(i, file) {
									//alert(file.name + ' - ' + file.size);
									data.append('file['+i+']', file);
								});
								jQuery.ajax({
									url: 'index.php?D[PAGE]=set_file',
									data: data,
									cache: false,
									contentType: false,
									processData: false,
									type: 'POST',
									success: function(data){
										  var dd = JSON.parse(data);
										 for(i=0;i <dd.length;i++)
											create_list('{$kTICKET}','{$MID}',dd[i].ID,dd[i].SEO_ID, dd[i].URLNAME, dd[i].NAME, parseInt(dd[i].SIZE));
									}
								});
							}
							document.getElementById('fileinput{$kTICKET}').addEventListener('change', handleFileSelect{$MID}, false);
							</script>
							<input type="file" name="files[]" id="fileinput{$kTICKET}"  multiple="multiple" accept="*/*" style="display:none;" />
							
						</div>
					</form>
				</li>
			</ul>

		{*</div>
		
		<div class="col-xs-3">
			Besitzer:
			Gruppe:
			Status:
			Priorität:
		</div>
	</div>*}

</div>
{/function}
{switch $D['ACTION']}
{case 'add_ticket'}
	{get_ticket kTICKET=$D.HELPDESK.D.TICKET.W.ID TICKET=$D.HELPDESK.D.TICKET.D[ $D.HELPDESK.D.TICKET.W.ID ]}
	{/case}
	{default}
<script>
var selectet_group_id = '{$D.HELPDESK.D.TICKET.W.GROUP_ID}';
get_ticket = function(start,stepp)
{
	start = (start)?start:0;
	stepp = (stepp)?stepp:10;
	$.ajax({ type: 'POST', 
	url : '?D[PAGE]=admin__helpdesklist&D[ACTION]=get_ticket'
	+ ((document.getElementById('D[HELPDESK][D][TICKET][W][ACTIVE]').value != '')?'&D[HELPDESK][D][TICKET][W][ACTIVE]='+document.getElementById('D[HELPDESK][D][TICKET][W][ACTIVE]').value:'' )
	+ ((selectet_group_id != '')?'&D[HELPDESK][D][TICKET][W][GROUP_ID]='+selectet_group_id:'' )
	,
	data:{
		'D[HELPDESK][D][TICKET][W][MESSAGE_TITLE|MESSAGE_TEXT]' : document.getElementById('D[HELPDESK][D][TICKET][W][MESSAGE_TITLE|MESSAGE_TEXT]').value,
		'D[HELPDESK][D][TICKET][L][START]' : start,
		'D[HELPDESK][D][TICKET][L][STEP]' : stepp
		//'D[HELPDESK][D][TICKET][W][ACTIVE]' : document.getElementById('D[HELPDESK][D][TICKET][W][ACTIVE]').value,
		//'D[HELPDESK][D][TICKET][W][GROUP_ID]' : document.getElementById('D[HELPDESK][D][TICKET][W][GROUP_ID]').value
		} 
	}).done(function (html) {
			$('#page').html(html);
		});
}
show_form = function(ticket_id,to)
{
	
	if(to)
	{
		$('#frame_form_'+ticket_id).show();
		$('#from_to_'+ticket_id).show();
		document.getElementById('D[HELPDESK][D][TICKET][D]['+ticket_id+'][TO]').value = to;
	}
	else //is notice
	{
		$('#frame_form_'+ticket_id).toggle();
		$('#from_to_'+ticket_id).hide();
		document.getElementById('D[HELPDESK][D][TICKET][D]['+ticket_id+'][TO]').value = '';
	}
}

add_ticket = function()
{
	$.ajax({ type: 'POST', 
		url : '?D[PAGE]=admin__helpdesklist&D[ACTION]=add_ticket'
		}).done(function (html) {
				$('#ticket_list').prepend(html);
			});
}
</script>

<div class="tab-content">
	<ul class="nav nav-tabs" role="tablist">
		<li data-toggle="tab" class="{if $D.HELPDESK.D.TICKET.W.GROUP_ID == ''}active{/if}" onclick="selectet_group_id='';get_ticket(0,10);return false;"><a href="#0" data-toggle="tab">alle <span class="badge">{if $D.HELPDESK.D.TICKET.W.GROUP_ID == ''}{$D.HELPDESK.D.TICKET.COUNT}{/if}</span></a></li>
		{foreach from=$D.HELPDESK.D.GROUP.D name=GROUP item=GROUP key=kGROUP}
			{if $GROUP.ACTIVE}
			<li data-toggle="tab" class="{if $D.HELPDESK.D.TICKET.W.GROUP_ID == $kGROUP}active{/if}" onclick="selectet_group_id='{$kGROUP}';get_ticket(0,10);return false;"><a href="#{$kGROUP}" data-toggle="tab">{$GROUP.TITLE} <span class="badge">{if $D.HELPDESK.D.TICKET.W.GROUP_ID == $kGROUP}{$D.HELPDESK.D.TICKET.COUNT}{/if}</span></a></li>
			{/if}
		{/foreach}
		<ul>
			<button type="button" title="Ticket erstellen" class="btn btn-default  pull-right" onclick="add_ticket()">+</button>
			<form id="form" class="navbar-form pull-right" style="margin:auto;">
				
				<button type="button" class="btn btn-default" title="E-Mail check" onclick="$.ajax({ url:'?D[PAGE]=admin__helpdeskcronjob'});return false;"><i class="fa fa-refresh"></i></button>
				<div class="input-group">
					<span class="input-group-btn">
						<select id="D[HELPDESK][D][TICKET][W][ACTIVE]" class="form-control">
							<option value="" {if $D.HELPDESK.D.TICKET.W['ACTIVE'] == ''}selected{/if}>alle</option>
							<option value="1" {if $D.HELPDESK.D.TICKET.W['ACTIVE'] == '1'}selected{/if}>offen</option>
							<option value="0" {if $D.HELPDESK.D.TICKET.W['ACTIVE'] == '0'}selected{/if}>geschloßen</option>
						</select>
						<input type="text" id="D[HELPDESK][D][TICKET][W][MESSAGE_TITLE|MESSAGE_TEXT]" name="D[HELPDESK][D][TICKET][W][MESSAGE_TITLE|MESSAGE_TEXT]" value="{$D.HELPDESK.D.TICKET.W['MESSAGE_TITLE|MESSAGE_TEXT']}" class="form-control" placeholder="Search for...">
						<button class="btn btn-default" type="button"  onclick="get_ticket(0,10);"><i class="fa fa-search" aria-hidden="true"></i></button>
					</span>
				</div>
			</form>
			
		</ul>
	</ul>
</div>



<section class="content" id="ticket_list">

			{foreach from=$D.HELPDESK.D.TICKET.D name=TICKET item=TICKET key=kTICKET}
				{get_ticket kTICKET=$kTICKET TICKET=$TICKET}
			{/foreach}
			<script>
				create_list = function(ticket_id, mid, id, seo_id, urlname, name, size)
				{
					$('#fl_list'+ticket_id).append("<li id='file_"+mid+""+id+"'>"
					+"<div style='width:100%;' class='input-group'>"
					+"	<span class='input-group-addon'><i class='fa fa-paperclip'></i></span>"
					+"	<span class='form-control'><a href='data/"+urlname+"' target='_blank'>"+name+"</a> ("+parseInt(size/1024)+"kb)</span>"
					+"	<span class='input-group-btn'><button onclick=\"$('#file_"+mid+""+id+"').remove();\" class='btn btn-default'><i class='fa fa-trash-o'></i></button></span>"
					+"</div>"
					+"<input type='hidden' name='D[HELPDESK][D][TICKET][D]["+ticket_id+"][MESSAGE][D]["+mid+"][FILE_SEONAME][D]["+seo_id+"][PATH]' value='tmp/data/"+urlname+"'></li>"
					);
				}
			</script>
						<script>
			$(".list-auto-scroll-down").each(function() {
			$(this).scrollTop($(this).height()*2);
				});
			</script>
		<nav aria-label="Page navigation">
			<ul class="pagination">
				<li onclick="get_ticket(0,10);return false;"><a href="#" aria-label="Previous"><i class="fa fa-fast-backward" aria-hidden="true"></i>&nbsp;</a></li>
				{section name=nav loop=($D.HELPDESK.D.TICKET.COUNT/10)|ceil step=1}
				<li onclick="get_ticket({$smarty.section.nav.index*10},10);return false;" ><a href="#">{$smarty.section.nav.index+1}</a></li>
				{/section}
				<li onclick="get_ticket({((($D.HELPDESK.D.TICKET.COUNT/10)|ceil)-1)*10},10);return false;"><a href="#" aria-label="Next"><i class="fa fa-fast-forward" aria-hidden="true"></i>&nbsp;</a></li>
			</ul>
		</nav>
</section>

{/switch}
{/block}