<script>
var selectet_group_id = '{$D.HELPDESK.D.TICKET.W.GROUP_ID}';
get_ticket = function(start,stepp)
{
	start = (start)?start:0;
	stepp = (stepp)?stepp:10;
	$.ajax({ type: 'POST', 
	url : '?D[PAGE]=admin__helpdesklist&D[ACTION]=get_ticket'
	+ ((document.getElementById('D[HELPDESK][D][TICKET][W][ACTIVE]').value != '')?'&D[HELPDESK][D][TICKET][W][ACTIVE]='+document.getElementById('D[HELPDESK][D][TICKET][W][ACTIVE]').value:'' )
	+ ((selectet_group_id != '')?'&D[HELPDESK][D][TICKET][W][GROUP_ID]='+selectet_group_id:'' )
	,
	data:{
		'D[HELPDESK][D][TICKET][W][MESSAGE_TITLE|MESSAGE_TEXT]' : document.getElementById('D[HELPDESK][D][TICKET][W][MESSAGE_TITLE|MESSAGE_TEXT]').value,
		'D[HELPDESK][D][TICKET][L][START]' : start,
		'D[HELPDESK][D][TICKET][L][STEP]' : stepp
		//'D[HELPDESK][D][TICKET][W][ACTIVE]' : document.getElementById('D[HELPDESK][D][TICKET][W][ACTIVE]').value,
		//'D[HELPDESK][D][TICKET][W][GROUP_ID]' : document.getElementById('D[HELPDESK][D][TICKET][W][GROUP_ID]').value
		} 
	}).done(function (html) {
			$('#page').html(html);
		});
}
show_form = function(ticket_id,to)
{
	
	if(to)
	{
		$('#frame_form_'+ticket_id).show();
		$('#from_to_'+ticket_id).show();
		document.getElementById('D[HELPDESK][D][TICKET][D]['+ticket_id+'][TO]').value = to;
	}
	else //is notice
	{
		$('#frame_form_'+ticket_id).toggle();
		$('#from_to_'+ticket_id).hide();
		document.getElementById('D[HELPDESK][D][TICKET][D]['+ticket_id+'][TO]').value = '';
	}
}
</script>

<section class="content">

	<div class="row">
		<div class="col-xs-9">
			<div class="panel {if $TICKET.ACTIVE}panel-info{else}panel-default{/if}">
				<div class="panel-heading">
					{if $TICKET.ACTIVE}<i class="fa fa-envelope-open-o"></i>{else}<i class="fa fa-envelope-o"></i>{/if}
					{$TICKET.DATETIME|date_format:"%d.%m.%Y %H:%M"}
				</div>
				
				<ul class="list-group">
					<li class="list-group-item no-padding">
						<div style="clear:both;max-height:200px;overflow-y:scroll;">
							<table class="table table-hover no-margin">
								{foreach from=$D.HELPDESK.D.TICKET.D[$kTICKET].MESSAGE.D name=MES item=MES key=kMES}
								<tr style="cursor:pointer;" class="{if !$MES.TO}list-group-item-warning{/if}" onclick="$('#frame{$kTICKET}').show();{if $MES.WAY == 'in'}show_form('{$kTICKET}','{$MES.FROM}');{else}$('#frame_form_{$kTICKET}').hide();{/if} document.getElementById('iframe{$kTICKET}').src='?D[PAGE]=admin__helpdesklist&D[ACTION]=get_message&D[HELPDESK][D][MESSAGE][W][ID]={$kMES}&D[HELPDESK][D][MESSAGE][W][TICKET_ID]={$kTICKET}';$('#fl_list{$kTICKET}').empty();">
									<td>{$smarty.foreach.MES.iteration}</td>
									<td><i class="fa {if $MES.WAY == 'in'}fa-chevron-down{elseif $MES.WAY == 'out'}fa-chevron-up{else}fa-commenting-o{/if}"></i></td>
									<td>{if $MES.USER_ID && $MES.WAY == 'out'}<i class="fa fa-user" title="{$MES.USER.NICK}"></i> {$MES.USER.NICK|truncate:4:"":false}{/if}</td>
									<td>{$MES.FROM}</td>
									<td>{$MES.TITLE|wordwrap:50:" ":true|truncate:100:"...":false}</td>
									<td style="width:150px;">{$MES.DATETIME|date_format:"%d.%m.%Y %H:%M"}</td>
								</tr>
								{$TITLE = $MES.TITLE}
								{*if $MES.FROM AND $MES.WAY == 'in'}{$FROM = $MES.FROM}{/if*}
								{if $MES.TO AND $MES.WAY == 'in'}{$TO = $MES.TO}{/if}
								{/foreach}
							</table>
						</div>
				
					</li>
					<li class="list-group-item no-padding" id="frame{$kTICKET}" style="display:none;">
						<button type="botton" class="btn btn-xs pull-right" onclick="$('#frame{$kTICKET}').hide();$('#frame_form_{$kTICKET}').hide()" style="position:absolute;right:0px;"><i class="fa fa-times"></i></button>
						<iframe id="iframe{$kTICKET}" style="width:100%;height:300px;border:none;" src=""></iframe>
					</li>
					<li class="list-group-item no-padding" id="frame_form_{$kTICKET}" style="display:;">
					
						{$MID = $smarty.now} 
						<form id="form{$kTICKET}" method="post" enctype="multipart/form-data">
							
								{*<input type="hidden" name="D[HELPDESK][D][TICKET][D][{$kTICKET}][GROUP_ID]" value="{$TICKET.GROUP_ID}">*}
								<div class="input-group" id="from_to_{$kTICKET}">
								
									<span style="min-width:100px;" class="input-group-addon">
										
									</span>
									<span style="min-width:100px;" class="input-group-addon">Von</span>
									<select name="D[HELPDESK][D][TICKET][D][{$kTICKET}][MESSAGE][D][{$MID}][FROM_ID]" class="form-control">
									{foreach from=$D.SETTING.HELPDESK.ACCOUNT.D name=ACC item=ACC key=kACC}
										{if $ACC.ACTIVE == 1}
										<option value="{$kACC}" {if $ACC.OUT.EMAIL == $TO}selected{/if}>{$ACC.TITLE} [{$ACC.OUT.EMAIL}]</option>
										{/if}
									{/foreach}
									</select>
									
									<span style="min-width:100px;" class="input-group-addon">An</span>
									<input id="D[HELPDESK][D][TICKET][D][{$kTICKET}][TO]" name="D[HELPDESK][D][TICKET][D][{$kTICKET}][MESSAGE][D][{$MID}][TO]" class="form-control" value="">
								</div>
								<div class="input-group">
									<span style="min-width:100px;" class="input-group-addon">Betreff</span>
									<input name="D[HELPDESK][D][TICKET][D][{$kTICKET}][MESSAGE][D][{$MID}][TITLE]" class="form-control" value="RE: Antwort">
								</div>
								{if $D.HELPDESK.D.GROUP.D[{$TICKET.GROUP_ID}].TEXTBLOCK.D}
								<div class="input-group">
									<span style="min-width:100px;" class="input-group-addon">Textbaustein</span>
									<select class="form-control" onchange="$.ajax({ url:'?D[PAGE]=admin__helpdesklist&D[ACTION]=get_textblock&D[HELPDESK][D][TEXTBLOCK][W][ID]='+this.value, data:'data', success: function(data){ $('#ta{$kTICKET}{$MID}').val(data); }}) ;//('#ta{$kTICKET}{$MID}').load('?D[PAGE]=helpdesklist&D[ACTION]=get_textblock&D[HELPDESK][D][TEXTBLOCK][W][ID]={$kTEX}');return false;">
										<option value='-'>Text Baustein</option>
										{foreach from=$D.HELPDESK.D.GROUP.D[{$TICKET.GROUP_ID}].TEXTBLOCK.D name=TEX item=TEX key=kTEX}
											{if $TEX.ACTIVE == 1}
											<option value="{$kTEX}">{$D.HELPDESK.D.TEXTBLOCK.D[{$kTEX}].TITLE}</option>
											{/if}
										{/foreach}
									</select>
								</div>
								{/if}
								<div class="input-group">
									<span style="min-width:100px;" class="input-group-addon">Text</span>
									<textarea id="ta{$kTICKET}{$MID}" name="D[HELPDESK][D][TICKET][D][{$kTICKET}][MESSAGE][D][{$MID}][TEXT]" class="form-control" style="height:150px;"></textarea>
								</div>
								<div class="input-group">
									<span class="input-group-btn">
									<button style="min-width:100px;" class="btn btn-default" onclick="$('#fileinput{$kTICKET}').trigger('click');" type="button"><i class="fa fa-plus"></i> Anlage</button>
									</span>
									<div style="height:auto;">
										<ul style="list-style:none;padding:0px;" id="fl_list{$kTICKET}"></ul>
									</div>
								</div>
								
							<div class="panel-footer">
								<div class="btn-group">
									<button class="btn btn-default" onclick="$.ajax({ type: 'POST', dataType : 'json', url : '?D[PAGE]=admin__helpdesklist&D[ACTION]=send_mail', data : $('#form{$kTICKET}').serialize()} );" type="button">Speichern</button>
									<button class="btn btn-default" onclick="show_form('{$kTICKET}','');" type="button" title="Notiz"><i class="fa fa-commenting-o"></i></button>
								</div>
								
								
								<script>
								//https://www.html5rocks.com/de/tutorials/file/dndfiles/
								handleFileSelect{$MID} = function(evt){
									//files = evt.target.files;
								
									var data = new FormData();
									jQuery.each(jQuery('#fileinput{$kTICKET}')[0].files, function(i, file) {
										//alert(file.name + ' - ' + file.size);
										data.append('file['+i+']', file);
									});
									jQuery.ajax({
										url: 'index.php?D[PAGE]=set_file',
										data: data,
										cache: false,
										contentType: false,
										processData: false,
										type: 'POST',
										success: function(data){
											  var dd = JSON.parse(data);
											 for(i=0;i <dd.length;i++)
												create_list('{$kTICKET}','{$MID}',dd[i].ID,dd[i].SEO_ID, dd[i].URLNAME, dd[i].NAME, parseInt(dd[i].SIZE));
										}
									});
								}
								document.getElementById('fileinput{$kTICKET}').addEventListener('change', handleFileSelect{$MID}, false);
								</script>
								<input type="file" name="files[]" id="fileinput{$kTICKET}"  multiple="multiple" accept="*/*" style="display:none;" />
								
							</div>
						</form>
					</li>
				</ul>

			</div>
		</div>
		<div class="col-xs-3">
			<div class="panel panel-info">
				<div class="panel-heading">Ticket Information</div>
				<div class="panel-body">
					Ticket-ID:<br>
					Status:<br>
					<select class="form-control" name="D[HELPDESK][D][TICKET][D][{$kTICKET}][ACTIVE]">
						{foreach from=$D.SETTING.HELPDESK.D.STATUS.D name=STA item=STA key=kSTA}
						<option value="{$kSTA}" {if $TICKET.ACTIVE == $kSTA}selected{/if}>{$STA.TITLE}</option>
						{/foreach}
					</select>
					Priorität:<br>
					<center>
					{for $i=0 to 4}
						<input type="radio" {if $i==2}checked{/if} value="{$i}" name="D[HELPDESK][D][TICKET][D][{$kTICKET}][PRIORITY]">
					{/for}
					</center>
					Gruppe:<br>
					<select title="Gruppe" class="form-control" name="D[HELPDESK][D][TICKET][D][{$kTICKET}][GROUP_ID]">
						<option value="">---</option>
						{foreach from=$D.HELPDESK.D.GROUP.D name=GROUP item=GROUP key=kGROUP}
							{if $GROUP.ACTIVE == 1}
								<option value="{$kGROUP}" {if $TICKET.GROUP_ID == $kGROUP}selected{/if}>{$GROUP.TITLE}</option>
							{/if}
						{/foreach}
					</select>
					Besitzer:
					<select title="Gruppe" class="form-control" name="D[MODUL][][USER][D][TICKET][D][{$kTICKET}][GROUP_ID]">
						<option value="">---</option>
						{foreach from=$D.USER.D name=USER item=USER key=kUSER}
							{if $GROUP.ACTIVE == 1}
								<option value="{$kUSER}" {if $TICKET.USER_ID == $kUSER}selected{/if}>{$USER.NICK}</option>
							{/if}
						{/foreach}
					</select>
					{*Beobachter:<br>*}
					Erstellungszeit: {$TICKET.ITIMESTAMP}<br>
				</div>
			</div>
			<div class="panel panel-info">
				<div class="panel-heading">Kunden Information</div>
				<div class="panel-body">
				</div>
			</div>
		</div>
	</div>
			<script>
				create_list = function(ticket_id, mid, id, seo_id, urlname, name, size)
				{
					$('#fl_list'+ticket_id).append("<li id='file_"+mid+""+id+"'>"
					+"<div style='width:100%;' class='input-group'>"
					+"	<span class='input-group-addon'><i class='fa fa-paperclip'></i></span>"
					+"	<span class='form-control'><a href='data/"+urlname+"' target='_blank'>"+name+"</a> ("+parseInt(size/1024)+"kb)</span>"
					+"	<span class='input-group-btn'><button onclick=\"$('#file_"+mid+""+id+"').remove();\" class='btn btn-default'><i class='fa fa-trash-o'></i></button></span>"
					+"</div>"
					+"<input type='hidden' name='D[HELPDESK][D][TICKET][D]["+ticket_id+"][MESSAGE][D]["+mid+"][FILE_SEONAME][D]["+seo_id+"][PATH]' value='tmp/data/"+urlname+"'></li>"
					);
				}
			</script>
</section>