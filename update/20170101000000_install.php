<?
class wp_helpdesk__20170101000000_install
{
	function __construct(){ global $C, $D; $this->C = &$C; $this->D = &$D; }
	function __call($m, $a){ return $a[0]; } 
	
	function up()
	{
		$this->C->db()->query("CREATE TABLE `helpdesk_file_to_message` (
		  `file_id` varchar(32) NOT NULL,
		  `message_id` varchar(32) NOT NULL,
		  `active` tinyint(1) DEFAULT NULL,
		  `itimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
		  `utimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci");

		$this->C->db()->query("CREATE TABLE `helpdesk_group` (
		  `id` varchar(32) NOT NULL,
		  `active` tinyint(1) DEFAULT NULL,
		  `title` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
		  `itimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
		  `utimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci");

		$this->C->db()->query("CREATE TABLE IF NOT EXISTS `helpdesk_group_to_textblock` (
		  `group_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
		  `textblock_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
		  `active` tinyint(1) DEFAULT NULL,
		  `utimestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  `itimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
		  PRIMARY KEY (`group_id`,`textblock_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");

		$this->C->db()->query("CREATE TABLE `helpdesk_message_file_seoname` (
		  `message_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
		  `file_seoname_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
		  `active` tinyint(1) DEFAULT NULL,
		  `itimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
		  `utimestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");

		$this->C->db()->query("CREATE TABLE `helpdesk_group_to_user` (
		  `group_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
		  `user_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
		  `active` tinyint(1) NOT NULL,
		  `utimestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  `itimestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci");

		$this->C->db()->query("CREATE TABLE `helpdesk_message` (
		  `id` varchar(32) NOT NULL,
		  `ticket_id` varchar(32) NOT NULL,
		  `user_id` varchar(32) DEFAULT NULL,
		  `active` tinyint(1) DEFAULT NULL,
		  `way` varchar(3) DEFAULT NULL,
		  `to` varchar(250) NOT NULL,
		  `from` varchar(250) DEFAULT NULL,
		  `title` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
		  `text` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
		  `datetime` datetime DEFAULT NULL,
		  `itimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
		  `utimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci");

		$this->C->db()->query("CREATE TABLE `helpdesk_textblock` (
		  `id` varchar(32) NOT NULL,
		  `active` tinyint(1) DEFAULT NULL,
		  `title` varchar(200) DEFAULT NULL,
		  `text` text,
		  `utimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  `itimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci");

		$this->C->db()->query("CREATE TABLE `helpdesk_ticket` (
		  `id` varchar(32) NOT NULL,
		  `group_id` varchar(32) DEFAULT NULL,
		  `number` int(11) NOT NULL,
		  `active` tinyint(1) DEFAULT NULL,
		  `datetime` datetime DEFAULT NULL,
		  `itimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
		  `utimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci");

		$this->C->db()->query("ALTER TABLE `helpdesk_message_file_seoname` ADD PRIMARY KEY (`message_id`,`file_seoname_id`);");
		$this->C->db()->query("ALTER TABLE `helpdesk_group` ADD PRIMARY KEY (`id`)");
		$this->C->db()->query("ALTER TABLE `helpdesk_group_to_textblock` ADD PRIMARY KEY (`textblock_id`,`group_id`)");
		$this->C->db()->query("ALTER TABLE `helpdesk_group_to_user` ADD PRIMARY KEY (`group_id`,`user_id`)");
		$this->C->db()->query("ALTER TABLE `helpdesk_message` ADD PRIMARY KEY (`id`,`ticket_id`), ADD KEY `way` (`way`), ADD KEY `user_id` (`user_id`)");
		$this->C->db()->query("ALTER TABLE `helpdesk_textblock` ADD PRIMARY KEY (`id`)");
		$this->C->db()->query("ALTER TABLE `helpdesk_ticket` ADD PRIMARY KEY (`id`) USING BTREE, ADD KEY `number` (`number`)");
		$this->C->db()->query("ALTER TABLE `helpdesk_ticket` MODIFY `number` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1");
		return 1;
	}
	
	function down()
	{
		return 1;
	}
}