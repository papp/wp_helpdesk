<?
class wp_helpdesk__20170727000000_update
{
	function __construct(){ global $C, $D; $this->C = &$C; $this->D = &$D; }
	function __call($m, $a){ return $a[0]; } 
	
	function up()
	{
		$this->C->db()->query("ALTER TABLE `helpdesk_ticket` ADD `priority` TINYINT NULL AFTER `group_id`, ADD INDEX (`priority`);");
		$this->C->db()->query("ALTER TABLE `helpdesk_ticket` ADD `user_id` VARCHAR(32) NULL AFTER `group_id`;");
		return 1;
	}
	
	function down()
	{
		return 1;
	}
}