<?
class wp_helpdesk__admin__admin extends wp_helpdesk__admin__admin__parent
{
	function load($d = null)
	{
		parent::{__function__}();
		$this->D['NAVIGATION']['D']['LEFT']['D']['HELPDESK']['LANGUAGE']['D']['DE'] = [ 'AWESOME_ICON'	=> 'fa-comments', 'TITLE' => 'Help Desk'];
		/*$this->D['NAVIGATION']['D']['LEFT']['D']['HELPDESK']['D']['TICKET']['LANGUAGE']['D']['DE'] = array(
			'AWESOME_ICON'	=> 'fa-comments',
			'TITLE' => 'Neues Ticket',
			'LINK'	=> 'D[PAGE]=admin__helpdesk_ticket',
		);*/
		$this->D['NAVIGATION']['D']['LEFT']['D']['HELPDESK']['D']['HELPDESK']['LANGUAGE']['D']['DE'] = array(
			'AWESOME_ICON'	=> 'fa-comments',
			'TITLE' => 'Help Desk',
			'LINK'	=> 'D[PAGE]=admin__helpdesklist&D[HELPDESK][D][TICKET][W][ACTIVE]=1',
		);
		$this->D['NAVIGATION']['D']['LEFT']['D']['HELPDESK']['D']['HELPDESK_STATISTIC']['LANGUAGE']['D']['DE'] = array(
			'AWESOME_ICON'	=> 'fa-pie-chart',
			'TITLE' => 'Statistik',
			'LINK'	=> 'D[PAGE]=admin__helpdeskstatistic',
		);
		$this->D['NAVIGATION']['D']['LEFT']['D']['SYSTEM']['D']['HELPDESK_SETTING']['LANGUAGE']['D']['DE'] = array(
			'AWESOME_ICON'	=> 'fa-comments',
			'TITLE' => 'Help Desk',
			'LINK'	=> 'D[PAGE]=admin__helpdesksetting',
		);
	}
}