<?
class wp_helpdesk__admin__helpdesk_ticket extends wp_helpdesk__admin__helpdesk_ticket__parent
{
	function load($d = null)
	{
		parent::{__function__}();
		
		$this->C->user()->check_right(['RIGHT'=>'ADMIN']);
		switch($this->D['ACTION'])
		{
			case 'set_ticket':
				$this->C->helpdesk()->set_ticket();
				exit;
				break;
			case 'send_mail':
				$this->C->helpdesk()->send_mail();
				$this->C->helpdesk()->set_ticket();
				exit;
				break;
			default:
				$this->C->setting()->get_setting();
				$this->C->helpdesk()->get_group();
				$this->C->helpdesk()->get_textblock();
				$this->C->user()->get_user();
				break;
		}
	}
	
	function show($d=null)
	{
		$this->C->library()->smarty()->assign('D', $this->D);
		$this->C->library()->smarty()->display(__dir__.'/tpl/admin__helpdesk_ticket.tpl');
	}
}