<?
class wp_helpdesk__admin__helpdesklist extends wp_helpdesk__admin__helpdesklist__parent
{
	function load($d = null)
	{
		parent::{__function__}();
		
		$this->C->user()->check_right(['RIGHT'=>'ADMIN']);
		switch($this->D['ACTION'])
		{
			case 'add_ticket':
				$this->D['HELPDESK']['D']['TICKET']['W']['ID'] = time();
				$this->D['HELPDESK']['D']['TICKET']['D'][ $this->D['HELPDESK']['D']['TICKET']['W']['ID'] ]['ACTIVE'] = 1;
				$this->C->helpdesk()->get_group();
				$this->C->helpdesk()->get_textblock();
				$this->C->user()->get_user();
				break;
			case 'set_ticket':
				$this->C->helpdesk()->set_ticket();
				exit;
				break;
			case 'send_mail':
				$this->C->helpdesk()->send_mail();
				$this->C->helpdesk()->set_ticket();
				exit;
				break;
			case 'get_textblock':
				$this->C->helpdesk()->get_textblock();
				foreach((array)$this->D['HELPDESK']['D']['TEXTBLOCK']['D'] as $k => $v )
					echo $v['TEXT'];
				exit;
				break;
			case 'get_message':
				#$this->D['HELPDESK']['D']['MESSAGE']['W'] = " id IN ('{$this->D['MESSAGE_ID']}')";
				$this->C->helpdesk()->get_message();
				if($this->D['HELPDESK']['D']['MESSAGE']['D'][ $this->D['HELPDESK']['D']['MESSAGE']['W']['ID'] ]['FILE_SEONAME']['D'])
				{
					echo "<div style='border-bottom: dotted 1px #999'>";
					foreach($this->D['HELPDESK']['D']['MESSAGE']['D'][ $this->D['HELPDESK']['D']['MESSAGE']['W']['ID'] ]['FILE_SEONAME']['D'] AS $kf => $vk)
					{
						echo "<a href='data/{$vk['NAME']}' target='_blank'>{$vk['NAME']}</a> ".($this->D['MODUL']['D']['wp_data']['DATA']['D'][$kf]['SIZE'])."kb<br>";
					}
					echo "</div>";
				}
				if($this->D['HELPDESK']['D']['MESSAGE']['D'][ $this->D['HELPDESK']['D']['MESSAGE']['W']['ID'] ]['WAY'] == 'out')
					echo nl2br($this->D['HELPDESK']['D']['MESSAGE']['D'][ $this->D['HELPDESK']['D']['MESSAGE']['W']['ID'] ]['TEXT']);
				else
					echo $this->D['HELPDESK']['D']['MESSAGE']['D'][ $this->D['HELPDESK']['D']['MESSAGE']['W']['ID'] ]['TEXT'];
				exit;
				break;
			default:
				#$this->C->helpdesk()->update_mail();
				#$this->C->helpdesk()->set_ticket();
				if(!isset($this->D['HELPDESK']['D']['TICKET']['L']['START']))
					$this->D['HELPDESK']['D']['TICKET']['L'] = ['START' => 0, 'STEP' => 10];
				$this->C->setting()->get_setting();
				$this->C->helpdesk()->get_ticket();
				$this->D['HELPDESK']['D']['GROUP']['W']['USER_ID'] = $this->D['SESSION']['USER']['ID'];
				$this->C->helpdesk()->get_group();
				$this->C->helpdesk()->get_textblock();
				$this->C->user()->get_user();
				break;
		}
	}
	
	function show($d=null)
	{
		##$this->D['SMARTY'] = "admin__helpdesklist.tpl".((($this->D['SMARTY'])?'|':'').$this->D['SMARTY']);
		$this->D['TEMPLATE']['FILE'][] = basename(__file__,'.php').".tpl";
		
		parent::{__function__}();
		$this->C->template()->display();
		##$this->C->library()->smarty()->assign('D', $this->D);
		#exit($this->C->library()->smarty()->fetch('extends:'.$this->D['SMARTY']));
		##$this->C->library()->smarty()->display('extends:'.$this->D['SMARTY']);
	}
}